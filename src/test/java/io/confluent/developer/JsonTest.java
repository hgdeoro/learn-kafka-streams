package io.confluent.developer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class JsonTest {

    public static class Data {
        public long number;
    }

    @Test
    public void testDeserialization() throws IOException {
        byte[] serializedData = "{\"number\": 123}".getBytes(StandardCharsets.UTF_8);
        ObjectMapper objectMapper = new ObjectMapper();
        Data deserialized = objectMapper.readValue(serializedData, Data.class);
        assertThat(deserialized.number, equalTo(Long.valueOf(123)));
    }

    @Test
    public void testSerialization() throws IOException {
        Data data = new Data();
        data.number = 321;

        ObjectMapper objectMapper = new ObjectMapper();
        byte[] serializedData = objectMapper.writeValueAsBytes(data);

        byte[] expected = "{\"number\":321}".getBytes(StandardCharsets.UTF_8);
        assertThat(expected, equalTo(serializedData));
    }

}
