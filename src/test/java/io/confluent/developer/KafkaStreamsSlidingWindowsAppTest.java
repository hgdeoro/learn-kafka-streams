package io.confluent.developer;

import io.confluent.developer.detector.Detector;
import io.confluent.developer.tx.Transaction;
import io.confluent.developer.tx.TransactionSerde;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class KafkaStreamsSlidingWindowsAppTest {

    private final static String TEST_CONFIG_FILE = "configuration/test.properties";

    @Test
    public void testApp() throws IOException {

        final Properties props = new Properties();
        try (InputStream inputStream = new FileInputStream(TEST_CONFIG_FILE)) {
            props.load(inputStream);
        }

        final String inputTopicName = props.getProperty("input.topic.name");
        final String outputTopicName = props.getProperty("output.topic.name");

        final Topology topology = KafkaStreamsSlidingWindowsApp.buildTopology(inputTopicName, outputTopicName);

        try (final TopologyTestDriver testDriver = new TopologyTestDriver(topology, props)) {
            Serde<String> keySerde = Serdes.String();
            Serde<String> evaluationResultSerde = Serdes.String();
            Serde<Double> doubleSerde = Serdes.Double();
            Serde<Transaction> messageSerde = new TransactionSerde();

            final TestInputTopic<String, Transaction> inputTopic = testDriver
                    .createInputTopic(inputTopicName, keySerde.serializer(), messageSerde.serializer());
            final TestOutputTopic<String, String> outputTopic = testDriver
                    .createOutputTopic(outputTopicName, keySerde.deserializer(), evaluationResultSerde.deserializer());

            Instant now = Instant.now();

            List<Transaction> inputs = Arrays.asList(
                    new Transaction("333", 1L, 100L),
                    new Transaction("333", 2L, 50L),

                    new Transaction("444", 1L, 1000L),
                    new Transaction("444", 2L, -500L),
                    new Transaction("444", 3L, -500L),

                    // Make time-window move, that way messages are emitted for ongoing aggregations
                    new Transaction("888", now.toEpochMilli(), 999L)
            );

            for(Transaction transaction : inputs) {
                inputTopic.pipeInput(transaction.getAccount().toString(), transaction);
            }

            final List<KeyValue<String, String>> actualOutputs = outputTopic.readKeyValuesToList();

            assertThat(actualOutputs.size(), equalTo(6));

            final List<String> detectedFrauds = new ArrayList<>();
            for(KeyValue<String, String> kv: actualOutputs) {
                if (Detector.FRAUD.equals(kv.value)) {
                    detectedFrauds.add(kv.key);
                } else if (Detector.OK.equals(kv.value)) {
                    // OK
                } else {
                    throw new RuntimeException("Invalid evaluation result: '" + kv.value + "'");
                }
            }

            assertThat(detectedFrauds, equalTo(List.of("444")));

        }

    }
}
