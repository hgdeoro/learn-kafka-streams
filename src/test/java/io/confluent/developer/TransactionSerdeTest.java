package io.confluent.developer;

import io.confluent.developer.tx.Transaction;
import io.confluent.developer.tx.TransactionDes;
import io.confluent.developer.tx.TransactionSer;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TransactionSerdeTest {

    @Test
    public void testCustomDes()  {
        TransactionDes des = new TransactionDes();
        String serialized = "{\"account\":\"333\",\"timestamp\":1672534800,\"amount\":987}";
        Transaction transaction = des.deserialize("topic", serialized.getBytes(StandardCharsets.UTF_8));

        assertThat(transaction, equalTo(new Transaction("333", 1672534800L, 987L)));
    }

    @Test
    public void testCustomSer()  {
        Transaction transaction = new Transaction("333", 1672534800L, 456L);

        TransactionSer ser = new TransactionSer();
        byte[] serialized = ser.serialize("topic", transaction);

        String expected = "{\"account\":\"333\",\"timestamp\":1672534800,\"amount\":456}";

        assertThat(new String(serialized), equalTo(expected));
    }

}
