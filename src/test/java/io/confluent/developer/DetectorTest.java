package io.confluent.developer;

import io.confluent.developer.detector.Detector;
import io.confluent.developer.detector.DetectorDes;
import io.confluent.developer.detector.DetectorSer;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DetectorTest {

    @Test
    public void testDetectorDes()  {
        DetectorDes des = new DetectorDes();
        String serialized = "{\"amounts\":[1,2,3]}";
        Detector detector = des.deserialize("topic", serialized.getBytes(StandardCharsets.UTF_8));

        assertThat(detector.getAmounts(), equalTo(List.of(1L, 2L, 3L)));
    }

    @Test
    public void testDetectorSer()  {
        Detector detector = new Detector();
        detector.setAmounts(List.of(4L,5L,6L));

        DetectorSer ser = new DetectorSer();
        byte[] serialized = ser.serialize("topic", detector);

        String expected = "{\"amounts\":[4,5,6]}";

        assertThat(new String(serialized), equalTo(expected));
    }

    @Test
    public void testDetectsValidMovements()  {
        Detector detector = new Detector();
        detector.setAmounts(List.of(1000L, -500L));

        assertThat(Detector.OK, equalTo(detector.evaluate()));
    }

    @Test
    public void testDetectsFraud()  {
        Detector detector = new Detector();
        detector.setAmounts(List.of(1000L, -500L, -500L));

        assertThat(Detector.FRAUD, equalTo(detector.evaluate()));
    }

}
