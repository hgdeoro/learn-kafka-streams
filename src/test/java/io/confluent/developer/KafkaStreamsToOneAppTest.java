package io.confluent.developer;

import io.confluent.developer.tx.Transaction;
import io.confluent.developer.tx.TransactionSerde;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class KafkaStreamsToOneAppTest {

    private final static String TEST_CONFIG_FILE = "configuration/test.properties";

    @Test
    public void testApp() throws IOException {

        final Properties props = new Properties();
        try (InputStream inputStream = new FileInputStream(TEST_CONFIG_FILE)) {
            props.load(inputStream);
        }

        final String inputTopicName = props.getProperty("input.topic.name");
        final String outputTopicName = props.getProperty("output.topic.name");

        final Topology topology = KafkaStreamsToOneApp.buildTopology(inputTopicName, outputTopicName);

        try (final TopologyTestDriver testDriver = new TopologyTestDriver(topology, props)) {
            Serde<String> keySerde = Serdes.String();
            Serde<Transaction> messageSerde = new TransactionSerde();

            final TestInputTopic<String, Transaction> inputTopic = testDriver
                    .createInputTopic(inputTopicName, keySerde.serializer(), messageSerde.serializer());
            final TestOutputTopic<String, Transaction> outputTopic = testDriver
                    .createOutputTopic(outputTopicName, keySerde.deserializer(), messageSerde.deserializer());

            List<Transaction> inputs = Arrays.asList(
                    new Transaction("333", 1672534800L, 987L),
                    new Transaction("333", 1672534801L, -987L)
            );
            List<Transaction> expectedOutputs = Arrays.asList(
                    new Transaction("333", 1672534800L, 1L),
                    new Transaction("333", 1672534801L, -1L)
            );

            inputs.forEach(inputTopic::pipeInput);
            final List<Transaction> actualOutputs = outputTopic.readValuesToList();

            assertThat(expectedOutputs, equalTo(actualOutputs));

        }

    }
}
