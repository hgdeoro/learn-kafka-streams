package io.confluent.developer.tx;

import io.confluent.developer.tx.Transaction;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TransactionDes implements Deserializer<Transaction> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Deserializer.super.configure(configs, isKey);
    }

    @Override
    public Transaction deserialize(String topic, byte[] data) {
        try {
            return new ObjectMapper().readValue(data, Transaction.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Transaction deserialize(String topic, Headers headers, byte[] data) {
        return Deserializer.super.deserialize(topic, headers, data);
    }

    @Override
    public void close() {
        Deserializer.super.close();
    }
}
