package io.confluent.developer.tx;

import java.util.Objects;

public class Transaction {
    private String account;
    private Long timestamp;
    private Long amount;

    public Transaction() {}

    public Transaction(String account, Long timestamp, Long amount) {
        this.account = account;
        this.timestamp = timestamp;
        this.amount = amount;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Transaction toOne() {
        if (this.amount >= 0)
            return new Transaction(this.account, this.timestamp, 1L);
        else
            return new Transaction(this.account, this.timestamp, -1L);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction transaction = (Transaction) o;
        return Objects.equals(account, transaction.account) && Objects.equals(timestamp, transaction.timestamp) && Objects.equals(amount, transaction.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, timestamp, amount);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "account=" + account +
                ", timestamp=" + timestamp +
                ", amount=" + amount +
                '}';
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
