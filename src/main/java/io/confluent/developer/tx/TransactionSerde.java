package io.confluent.developer.tx;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class TransactionSerde implements Serde<Transaction> {
    @Override
    public Serializer<Transaction> serializer() {
        return new TransactionSer();
    }

    @Override
    public Deserializer<Transaction> deserializer() {
        return new TransactionDes();
    }
}
