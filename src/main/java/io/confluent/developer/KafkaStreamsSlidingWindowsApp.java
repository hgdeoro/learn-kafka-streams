package io.confluent.developer;

import io.confluent.developer.detector.Detector;
import io.confluent.developer.detector.DetectorSerde;
import io.confluent.developer.tx.Transaction;
import io.confluent.developer.tx.TransactionSerde;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.TimestampExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.Duration;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import static org.apache.kafka.common.serialization.Serdes.Long;

/**
 * Uses a SlidingWindows to group TX and evaluates them.
 * <p>
 * Current implementation is the simplest one. Has some problems:
 * <p>
 * 1. It emits 'OK', they should be easy to filter out.
 * <p>
 * 2. If movements are +100, -100, +100, -100, fraud will be detected twice.
 */
public class KafkaStreamsSlidingWindowsApp {

    private static final Logger logger = LoggerFactory.getLogger(KafkaStreamsSlidingWindowsApp.class);

    static class MessageTimestampExtractor implements TimestampExtractor {
        @Override
        public long extract(ConsumerRecord<Object, Object> record, long partitionTime) {
            Transaction transaction = (Transaction) record.value();
            logger.info("Extracting time of {} / Time={}", transaction, transaction.getTimestamp());
            return transaction.getTimestamp();
        }
    }

    static void runKafkaStreams(final KafkaStreams streams) {
        final CountDownLatch latch = new CountDownLatch(1);
        streams.setStateListener((newState, oldState) -> {
            if (oldState == KafkaStreams.State.RUNNING && newState != KafkaStreams.State.RUNNING) {
                latch.countDown();
            }
        });

        streams.start();

        try {
            latch.await();
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }

        logger.info("Streams Closed");
    }

    static Topology buildTopology(String inputTopic, String outputTopic) {
        Serde<String> keySerde = Serdes.String();
        Serde<Transaction> messageSerde = new TransactionSerde();
        Serde<Detector> detectorSerde = new DetectorSerde();

        StreamsBuilder builder = new StreamsBuilder();

        final KGroupedStream<String, Transaction> groupedStreamOfMessages = builder
                .stream(inputTopic, Consumed.with(keySerde, messageSerde).withTimestampExtractor(new MessageTimestampExtractor()))
                .peek((k, v) -> logger.info("Observed event: {}", v))
                .groupByKey();

        groupedStreamOfMessages
                .windowedBy(SlidingWindows.ofTimeDifferenceWithNoGrace(Duration.ofMinutes(30L)))
                .aggregate(
                        Detector::new,
                        (key, message, detector) -> {
                            detector.addMoneyMovement(message.getAmount());
                            return detector;
                        },
                        Materialized.with(Serdes.String(), detectorSerde)
                )
                .suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded()))
                .toStream()
                .map((wk, value) -> KeyValue.pair(wk.key(), value.evaluate()))
                // .mapValues(value -> value.result(), Materialized.as(""))
                .peek((key, value) -> logger.info("Outgoing record - key={} value={}", key, value))
                .to(outputTopic, Produced.with(Serdes.String(), Serdes.String()));

        return builder.build();
    }

    public static void runner(String args) throws Exception {
        Properties props = new Properties();
        try (InputStream inputStream = new FileInputStream(args)) {
            props.load(inputStream);
        }

        final String inputTopic = props.getProperty("input.topic.name");
        final String outputTopic = props.getProperty("output.topic.name");

        try (Util utility = new Util()) {
            utility.createTopics(
                    props,
                    Arrays.asList(
                            new NewTopic(inputTopic, Optional.empty(), Optional.empty()),
                            new NewTopic(outputTopic, Optional.empty(), Optional.empty())));

            KafkaStreams kafkaStreams = new KafkaStreams(
                    buildTopology(inputTopic, outputTopic),
                    props);

            Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));

            logger.info("KafkaStreamsSlidingWindowsApp Started");
            runKafkaStreams(kafkaStreams);
        }
    }

}
