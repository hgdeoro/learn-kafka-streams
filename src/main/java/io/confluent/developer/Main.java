package io.confluent.developer;

public class Main {
    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            throw new IllegalArgumentException("This program takes 2: command + the path to a configuration file.");
        }

        if (args[0].equals("KafkaStreamsApplication-generate")) {
            KafkaStreamsApplication.generator(args[1]);
        } else if (args[0].equals("KafkaStreamsApplication-run")) {
            KafkaStreamsApplication.runner(args[1]);
        } else if (args[0].equals("KafkaStreamsSlidingWindowsApp-run")) {
            KafkaStreamsSlidingWindowsApp.runner(args[1]);
        } else {
            throw new RuntimeException("Invalid action: " + args[0]);
        }

    }
}