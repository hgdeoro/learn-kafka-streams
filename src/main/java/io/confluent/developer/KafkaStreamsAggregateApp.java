package io.confluent.developer;

import io.confluent.developer.tx.Transaction;
import io.confluent.developer.tx.TransactionSerde;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.TimestampExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;

public class KafkaStreamsAggregateApp {

    private static final Logger logger = LoggerFactory.getLogger(KafkaStreamsAggregateApp.class);

    static class MessageTimestampExtractor implements TimestampExtractor {
        @Override
        public long extract(ConsumerRecord<Object, Object> record, long partitionTime) {
            Transaction transaction = (Transaction) record.value();
            logger.info("Extracting time of {} / Time={}", transaction, transaction.getTimestamp());
            return transaction.getTimestamp();
        }
    }

    static void runKafkaStreams(final KafkaStreams streams) {
        final CountDownLatch latch = new CountDownLatch(1);
        streams.setStateListener((newState, oldState) -> {
            if (oldState == KafkaStreams.State.RUNNING && newState != KafkaStreams.State.RUNNING) {
                latch.countDown();
            }
        });

        streams.start();

        try {
            latch.await();
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }

        logger.info("Streams Closed");
    }

    static Topology buildTopology(String inputTopic, String outputTopic) {
        Serde<String> keySerde = Serdes.String();
        Serde<Transaction> messageSerde = new TransactionSerde();
        StreamsBuilder builder = new StreamsBuilder();

        final KStream<String, Transaction> messageStream =
                builder.stream(inputTopic, Consumed.with(keySerde, messageSerde).withTimestampExtractor(new MessageTimestampExtractor()))
                        .peek((k, v) -> logger.info("Observed event: {}", v));

        messageStream
                .groupByKey()
                .windowedBy(TimeWindows.ofSizeWithNoGrace(Duration.ofHours(1)))
                .aggregate(
                        () -> 0.0,
                        (key, message, total) -> total + message.getAmount(),
                        Materialized.with(Serdes.String(), Serdes.Double())
                ).suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded()))
                .toStream()
                .map((wk, value) -> KeyValue.pair(wk.key(),value))
                .peek((key, value) -> logger.info("Outgoing record - key={} value={}", key, value))
                .to(outputTopic, Produced.with(Serdes.String(), Serdes.Double()));

        return builder.build();
    }

}
