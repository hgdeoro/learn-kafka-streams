package io.confluent.developer.detector;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class DetectorSerde implements Serde<Detector> {
    @Override
    public Serializer<Detector> serializer() {
        return new DetectorSer();
    }

    @Override
    public Deserializer<Detector> deserializer() {
        return new DetectorDes();
    }
}
