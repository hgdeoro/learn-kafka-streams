package io.confluent.developer.detector;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.Map;

public class DetectorDes implements Deserializer<Detector> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Deserializer.super.configure(configs, isKey);
    }

    @Override
    public Detector deserialize(String topic, byte[] data) {
        try {
            return new ObjectMapper().readValue(data, Detector.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Detector deserialize(String topic, Headers headers, byte[] data) {
        return Deserializer.super.deserialize(topic, headers, data);
    }

    @Override
    public void close() {
        Deserializer.super.close();
    }
}
