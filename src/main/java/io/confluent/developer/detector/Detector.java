package io.confluent.developer.detector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Detector {
    private static final Logger logger = LoggerFactory.getLogger(Detector.class);

    public static String FRAUD = "fraud";
    public static String OK = "ok";

    private final List<Long> amounts = new ArrayList<>();

    public void addMoneyMovement(Long amount) {
        // FIXME: add some defensive approach, log, even ignore, if there are too many amounts
        // FIXME: think an algorithm to make this more efficient. Maybe:
        // 1) count + max + min + sum of deposits
        // 2) count + max + min + sum of extractions
        // Make some algorithm with 1) and 2) to try to detect
        this.getAmounts().add(amount);
    }

    public String evaluate() {
        if (this.getAmounts().isEmpty()) {
            logger.info("Empty list of amounts -> OK");
            return OK;
        }

        Long sum = this.getAmounts().stream().reduce(0L, Long::sum); // FIXME: overflow
        if (sum == 0) { // Probably we should check "close" to zero
            logger.info("FRAUD >: {}", this.amounts);
            return FRAUD;
        } else {
            logger.info("OK :) {}", this.amounts);
            return OK;
        }
    }

    public void setAmounts(List<Long> amounts) {
        this.amounts.addAll(amounts);
    }

    public List<Long> getAmounts() {
        return amounts;
    }
}
