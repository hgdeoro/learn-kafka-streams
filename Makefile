GRADLEW := ./gradlew

docker-build:
	$(GRADLEW) \
		jibDockerBuild \
		--image=io.confluent.developer/creating-first-apache-kafka-streams-application-join:0.0.1

shadowJar:
	$(GRADLEW) \
		shadowJar

test:
	$(GRADLEW) \
		test

KafkaStreamsApplication-generate:
	java \
		-jar build/libs/creating-first-apache-kafka-streams-application-*.jar \
		KafkaStreamsApplication-generate \
		configuration/dev.properties

KafkaStreamsApplication-run:
	java \
		-jar build/libs/creating-first-apache-kafka-streams-application-*.jar \
		KafkaStreamsApplication-run \
		configuration/dev.properties

KafkaStreamsSlidingWindowsApp-run:
	java \
		-jar build/libs/creating-first-apache-kafka-streams-application-*.jar \
		KafkaStreamsSlidingWindowsApp-run \
		configuration/KafkaStreamsSlidingWindowsApp.properties

KafkaStreamsSlidingWindowsApp-reset:
	./venv/bin/docker-compose exec broker \
		/usr/bin/kafka-topics --bootstrap-server localhost:9092 --delete --topic tx || true
	./venv/bin/docker-compose exec broker \
		/usr/bin/kafka-topics --bootstrap-server localhost:9092 --delete --topic fraud-detection || true

venv:
	python3 -m venv venv
	./venv/bin/pip install docker-compose

kafka-up:
	./venv/bin/docker-compose \
		up

kafka-up-d:
	./venv/bin/docker-compose \
		up -d

kafka-logs:
	./venv/bin/docker-compose \
		logs

kafka-ps:
	./venv/bin/docker-compose \
		ps

list-topics:
	./venv/bin/docker-compose exec broker \
		/usr/bin/kafka-topics --bootstrap-server localhost:9092 --list

kafka-console-consumer-random-strings:
	./venv/bin/docker-compose exec broker \
		/usr/bin/kafka-console-consumer --topic random-strings --bootstrap-server broker:9092

produce-79825349:
	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":1676810383000,"amount":799}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":1676810384000,"amount":-700}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":1676810385000,"amount":-99}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":9999999999999,"amount":1}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx

produce-18324695:
	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":1676815555000,"amount":499}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":1676815556000,"amount":-400}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":1676815557000,"amount":-50}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":9999999999999,"amount":1}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx

produce-both:
	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":1676810383000,"amount":799}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":1676810384000,"amount":-700}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":1676810385000,"amount":-99}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx

	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":1676815555000,"amount":499}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":1676815556000,"amount":-400}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":1676815557000,"amount":-50}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx

	/usr/bin/echo -e '79825349\t{"account":"79825349","timestamp":9999999999998,"amount":1}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
	/usr/bin/echo -e '18324695\t{"account":"18324695","timestamp":9999999999999,"amount":1}' | ./venv/bin/docker-compose exec -T broker kafka-console-producer --bootstrap-server localhost:9092 --property parse.key=true  --topic tx
